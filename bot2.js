var 	irc = require('irc'),
	config = require('./config/botconfig'),
	Sleep = require('sleep'),
	dbaccess = require('./dbmodule'),
	confing = require('./config/botconfig');

var bot = new irc.Client(config.irc.server, config.irc.botName, {
} );


var outQueue = function(who, output ) {
	if( !Array.isArray(output ) ) {
		bot.say(who, output );
	} else {
		output.map( function(speech) {
			bot.say(who, speech );
			Sleep.usleep(800000 );
		} );
	}
};

var botQuit = function() {
	bot.disconnect("Bye!");
	pgp.end();
	process.exit();
};




bot.addListener( "motd", function( motd ) {
	bot.say( "nickserv", "identify " + config.irc.nickPass );
} );

bot.addListener( "error", function( message ) {
	console.log( message );
} );


bot.addListener( "notice", function(nick, to, text, message) {
	if( to === config.irc.botName) {
		if( nick.toLowerCase() === "nickserv") {
			Sleep.usleep(800000);
			bot.join( config.irc.channels[0] );
		}
	}
});

bot.addListener( "pm", function( from, message ) {

	words = message.split( ' ' );
	words[0] = words[0].toLowerCase();

	if( words[0] === "!quit" && from === config.irc.botOwner) {
		bot.disconnect( "Yes Boss!" );
		pgp.end();
		process.exit();
	}

} );


bot.addListener("raw", function(message) {
	words = message.args.slice(0);
	var botcommand = "";
	var botchannel = "";
	var temp = '';
 	if( message.command !== "PRIVMSG" ) {
		return;
	}

	if( words[0].startsWith('#') && words[1].startsWith('!') ) { //From a channel
		botchannel = words[0];
		temp = message.args[1].split(' ');
		botcommand = temp[0];
		words = temp.slice(1);

	}  else if( words[0] === config.irc.botName && words[1].startsWith('!') ) { //PM
		temp = words[1].split(' ');

		botcommand = temp[0];
		if(temp.length > 1) {
			words = temp.slice(1);
		}

	} else { // Channel text
		return;
	}

	dbaccess.botCommand( message.nick, message.host, botchannel, botcommand, words, outQueue);

} );


